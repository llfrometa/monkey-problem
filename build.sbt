name := "monkey-problem"

version := "1.0.0"

organization := "com.clluc"

scalaVersion := "2.12.1"


libraryDependencies ++= {
  val AKKA_VERSION = "2.4.17"
  val SCALA_TEST_VERSION = "3.0.1"
  val TEST_SCOPE = "test"

  Seq(
    "com.typesafe.akka" %% "akka-actor" % AKKA_VERSION,
    "com.typesafe.akka" %% "akka-testkit" % AKKA_VERSION % TEST_SCOPE,
    "org.scalatest" %% "scalatest" % SCALA_TEST_VERSION % TEST_SCOPE
  )
}