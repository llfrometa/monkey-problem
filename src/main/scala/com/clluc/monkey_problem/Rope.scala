package com.clluc.monkey_problem

import akka.actor.{Actor, ActorLogging, Props}

/** Rope Actor [[com.clluc.monkey_problem.Rope]] */
class Rope extends Actor with ActorLogging {

  import RopeProtocol._

  val ropeManager = new RopeManager

  override def receive: Receive = {
    case RegisterMonkey(monkeyId, direction) => ropeManager.registerMonkey(monkeyId, direction)
      log.debug(s"[Rope::RegisterMonkey] -> monkeyList = ${ropeManager.monkeyList}")
    case UnRegisterMonkey(monkeyId) => ropeManager.unRegisterMonkey(monkeyId)
      log.debug(s"[Rope::UnRegisterMonkey] -> monkeyList = ${ropeManager.monkeyList}")
    case IsBusy(direction) => sender() ! ropeManager.isBusy(direction)
  }
}

/** Companion object of Rope Actor [[com.clluc.monkey_problem.Rope]] */
object Rope {

  /** Good practice defining props method of the actor
    *
    * @return Props
    */
  def props: Props = Props(classOf[Rope])
}

/** Represent the object protocol of Rope Actor [[com.clluc.monkey_problem.RopeProtocol]] */
object RopeProtocol {

  case class RegisterMonkey(monkeyId: String, direction: Int)

  case class UnRegisterMonkey(monkeyId: String)

  case class IsBusy(direction: Int)

}

/** MonkeyRopeManager is object for managing the monkey in the rope [[com.clluc.monkey_problem.RopeManager]] */
class RopeManager {

  var monkeyList: List[(String, Int)] = List.empty[(String, Int)]

  /** Add new tuple (monkeyId, direction)
    *
    * @param monkeyId  The monkey identifier
    * @param direction The monkey direction [GO_TO_EAST, GO_TO_WEST]
    */
  def registerMonkey(monkeyId: String, direction: Int): Unit = {
    if (monkeyId.nonEmpty && (direction == 1 || direction == 0)) monkeyList = (monkeyId, direction) :: monkeyList
  }

  /** Remove tuple (monkeyId, direction)
    *
    * @param monkeyId The monkey identifier
    */
  def unRegisterMonkey(monkeyId: String): Unit = monkeyList = monkeyList.filter(_._1 != monkeyId)

  /** Check if exit monkey in the rope or exit any monkey in the same direction
    *
    * @param direction The monkey direction [GO_TO_EAST, GO_TO_WEST]
    */
  def isBusy(direction: Int): Boolean = monkeyList match {
    case Nil => false
    case head :: _ => if (head._2 == direction) false else true
  }
}
