package com.clluc.monkey_problem.unit_test

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{DefaultTimeout, ImplicitSender, TestKit, TestProbe}
import com.clluc.monkey_problem.MonkeyProtocol._
import com.clluc.monkey_problem.{Monkey, Rope}
import com.clluc.monkey_problem.RopeProtocol._
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll, FunSuiteLike, Matchers}

import scala.concurrent.duration._

class MonkeyTest extends TestKit(ActorSystem.create("ActorSystemMonkeyTest"))
  with FunSuiteLike with BeforeAndAfter with BeforeAndAfterAll with DefaultTimeout with ImplicitSender with Matchers {

  var testProbe: TestProbe = _
  var monkeyExpectMsg: ActorRef = _
  before {
    testProbe = TestProbe()
    monkeyExpectMsg = testProbe.ref
  }

  test("send StartMonkeyProcess SHOULD expect StartMonkeyProcess message") {
    monkeyExpectMsg ! StartMonkeyProcess(MonkeyInitialData(1, 3.seconds))
    testProbe.expectMsg(StartMonkeyProcess(MonkeyInitialData(1, 3.seconds)))
  }

  test("send UpRope SHOULD expect UpRope message") {
    monkeyExpectMsg ! UpRope
    testProbe.expectMsg(UpRope)
  }

  test("send WalkingInRope SHOULD expect WalkingInRope message") {
    monkeyExpectMsg ! WalkingInRope
    testProbe.expectMsg(WalkingInRope)
  }

  test("send Finalized SHOULD expect Finalized message") {
    monkeyExpectMsg ! Finalized
    testProbe.expectMsg(Finalized)
  }
}
