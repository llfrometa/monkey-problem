package com.clluc.monkey_problem.util

import java.util.UUID

/** Wrapper for UUID Generator [[com.clluc.monkey_problem.util.IdentifierGenerator]] */
object IdentifierGenerator {

  /** Generator string identifier
    *
    * @return String
    */
  def generate: String = UUID.randomUUID().toString
}
