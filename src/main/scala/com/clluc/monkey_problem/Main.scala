package com.clluc.monkey_problem

import akka.actor.ActorSystem
import com.clluc.monkey_problem.util.Config

object Main extends App with Config {

  import MonkeyManagerProtocol._

  final val NAME = "ActorSystemMonkey"
  val actorSystem: ActorSystem = ActorSystem.create(NAME)
  val monkeyManager = actorSystem.actorOf(MonkeyManager.props)
  monkeyManager ! Start(MONKEY_TOTAL)
}
