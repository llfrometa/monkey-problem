package com.clluc.monkey_problem.util

import akka.util.Timeout

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

/** Object Implicit Timeout for Future and ask method [[com.clluc.monkey_problem.util.TimeoutParameter]] */
object TimeoutParameter {

  object Implicit {
    implicit val executionContext: ExecutionContext = ExecutionContext.Implicits.global
    implicit lazy val timeout = Timeout(60 seconds)
  }

}
