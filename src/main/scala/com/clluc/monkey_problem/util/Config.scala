package com.clluc.monkey_problem.util

import com.typesafe.config.{Config => ConfigParent, ConfigFactory}

/** Object for read config file [application.config] [[com.clluc.monkey_problem.util.Config]] */
object Config {

  import ConfigKeyEnum._

  val config: ConfigParent = ConfigFactory.load
  val MONKEY_TOTAL: Int = config.getInt(MONKEY_TOTAL_KEY)
}

/** Trait configuration. Used for read one once the file config [[com.clluc.monkey_problem.util.Config]] */
trait Config {
  val MONKEY_TOTAL: Int = Config.MONKEY_TOTAL
}

/** Object key enum for names in file config [application.config] [[com.clluc.monkey_problem.util.ConfigKeyEnum]] */
object ConfigKeyEnum {
  final val MONKEY_TOTAL_KEY = "monkey-total"
}
