package com.clluc.monkey_problem.unit_test

import com.clluc.monkey_problem.RopeManager
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll, FunSuiteLike, Matchers}

class RopeManagerTest extends FunSuiteLike with BeforeAndAfter with BeforeAndAfterAll with Matchers {

  val monkeyId: String = "monkeyId"
  val directionGoToEast: Int = 1
  val directionGoToWest: Int = 0
  var ropeManager: RopeManager = _

  before {
    ropeManager = new RopeManager
  }

  test("register new monkey WHEN valid id and direction SHOULD return size equal 1") {
    ropeManager.registerMonkey(monkeyId, directionGoToEast)
    val size = ropeManager.monkeyList.size
    size shouldBe 1
  }

  test("register new monkey WHEN invalid id SHOULD return size equal 0") {
    val invalidMonkeyId: String = ""
    ropeManager.registerMonkey(invalidMonkeyId, directionGoToEast)
    val size = ropeManager.monkeyList.size
    size shouldBe 0
  }

  test("register new monkey WHEN invalid direction SHOULD return size equal 0") {
    val invalidDrection: Int = 3
    ropeManager.registerMonkey(monkeyId, invalidDrection)
    val size = ropeManager.monkeyList.size
    size shouldBe 0
  }

  test("unregister monkey WHEN valid id SHOULD return size equal 2") {
    val monkeyId: String = "monkeyId"
    val monkeyId2: String = "monkeyId2"
    val monkeyId3: String = "monkeyId3"
    val direction: Int = 1
    ropeManager.registerMonkey(monkeyId, direction)
    ropeManager.registerMonkey(monkeyId2, direction)
    ropeManager.registerMonkey(monkeyId3, direction)
    ropeManager.unRegisterMonkey(monkeyId)
    val size = ropeManager.monkeyList.size
    size shouldBe 2
  }

  test("unregister monkey WHEN invalid id SHOULD return size equal 3") {
    val monkeyId: String = "monkeyId"
    val monkeyId2: String = "monkeyId2"
    val monkeyId3: String = "monkeyId3"
    val monkeyId4: String = "monkeyId4"
    val direction: Int = 1
    ropeManager.registerMonkey(monkeyId, direction)
    ropeManager.registerMonkey(monkeyId2, direction)
    ropeManager.registerMonkey(monkeyId3, direction)
    ropeManager.unRegisterMonkey(monkeyId4)
    val size = ropeManager.monkeyList.size
    size shouldBe 3
  }

  test("is busy rope if one monkey can be up rope with direction 0 WHEN all monkey has direction 1 SHOULD return true") {
    val monkeyId: String = "monkeyId"
    val monkeyId2: String = "monkeyId2"
    val monkeyId3: String = "monkeyId3"
    val directionGotoEast: Int = 1
    val directionGotoWest: Int = 0
    ropeManager.registerMonkey(monkeyId, directionGotoEast)
    ropeManager.registerMonkey(monkeyId2, directionGotoEast)
    ropeManager.registerMonkey(monkeyId3, directionGotoEast)
    val result = ropeManager.isBusy(directionGotoWest)
    result shouldBe true
  }

  test("is busy rope WHEN empty monkeyList SHOULD return false") {
    val directionGotoWest: Int = 0
    val result = ropeManager.isBusy(directionGotoWest)
    result shouldBe false
  }

  test("is busy rope if one monkey can be up rope with direction 1 WHEN all monkey has direction 1 SHOULD return false") {
    val monkeyId: String = "monkeyId"
    val monkeyId2: String = "monkeyId2"
    val monkeyId3: String = "monkeyId3"
    val directionGotoEast: Int = 1
    ropeManager.registerMonkey(monkeyId, directionGotoEast)
    ropeManager.registerMonkey(monkeyId2, directionGotoEast)
    ropeManager.registerMonkey(monkeyId3, directionGotoEast)
    val result = ropeManager.isBusy(directionGotoEast)
    result shouldBe false
  }
}
