package com.clluc.monkey_problem

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.pattern.ask
import com.clluc.monkey_problem.RopeProtocol._
import com.clluc.monkey_problem.util.TimeoutParameter.Implicit._
import scala.concurrent.Future
import scala.concurrent.duration._

/** Monkey Actor [[com.clluc.monkey_problem.Monkey]] */
class Monkey(id: String, rope: ActorRef) extends Actor with ActorLogging {

  import MonkeyActionStatus._
  import MonkeyProtocol._

  var status: Status = StoppedStatus
  var direction: Int = _
  var senderRef: ActorRef = _

  override def receive: Receive = {
    case StartMonkeyProcess(monkeyData) =>
      senderRef = sender()
      startMonkeyProcess(monkeyData)
    case UpRope => startUpRope()
    case WalkingInRope => startWalkingInRope()
    case Finalized => finalized()
  }

  /** Start monkey process for start rope
    *
    * @param monkeyData The class MonkeyInitialData
    */
   def startMonkeyProcess(monkeyData: MonkeyInitialData): Unit = {
    direction = monkeyData.direction
    status = WalkingToStartStatus
    scheduleOnce(monkeyData.timeToStart)(self ! UpRope)
    log.info(s"[MONKEY] -> Monkey(id = $id, status = $status) => MonkeyData(direction = ${monkeyData.direction}, timeToStart = ${monkeyData.timeToStart}))")
  }

  /**
    * Check if the monkey can be start up the rope
    */
   def startUpRope(): Unit = {
    (rope ? IsBusy(direction)).map {
      case false =>
        status = UpRopeStatus
        scheduleOnce(1.seconds)(self ! WalkingInRope)
        log.info(s"[MONKEY][CHANGE STATUS] -> Monkey(id = $id, status = $status)")
      case true => scheduleOnce(1.seconds)(self ! UpRope)
    }.recoverWith {
      case e: Exception =>
        log.info(s"[MONKEY][ERROR] = $e")
        Future(true)
    }
  }

  /**
    * Start the monkey walking in the rope
    */
  private def startWalkingInRope(): Unit = {
    rope ! RegisterMonkey(id, direction)
    status = WalkingInRopeStatus
    scheduleOnce(4.seconds)(self ! Finalized)
    log.info(s"[MONKEY][CHANGE STATUS] -> Monkey(id = $id, status = $status)")
  }

  /**
    * Finish the monkey walking in the rope
    */
  private def finalized(): Unit = {
    rope ! UnRegisterMonkey(id)
    status = FinalizedStatus
    log.info(s"[MONKEY][CHANGE STATUS] -> Monkey(id = $id, status = $status)")
    senderRef ! id
  }

  /** Wrapper the function [context.system.scheduler.scheduleOnce]
    *
    * @param delay The time for execute the function
    * @param f The function for execute
    */
  private def scheduleOnce(delay: FiniteDuration)(f: ⇒ Unit): Unit = context.system.scheduler.scheduleOnce(delay)(f)

}

/** Companion object of Rope Actor [[com.clluc.monkey_problem.Monkey]] */
object Monkey {

  /** Good practice defining props method of the actor
    *
    * @return Props
    */
  def props(id: String, rope: ActorRef): Props = Props(classOf[Monkey], id, rope)
}

/** Represent the object protocol of Monkey Actor [[com.clluc.monkey_problem.Monkey]] */
object MonkeyProtocol {

  case class MonkeyInitialData(direction: Int, timeToStart: FiniteDuration)

  case class StartMonkeyProcess(monkeyData: MonkeyInitialData)

  case object UpRope

  case object WalkingInRope

  case object Finalized

}

/** Represent the object direction of Monkey [[com.clluc.monkey_problem.MonkeyDirection]] */
object MonkeyDirection {
  final val GO_TO_EAST = 1
  final val GO_TO_WEST = 0
}

/** Represent the object status of Monkey [[com.clluc.monkey_problem.MonkeyActionStatus]] */
object MonkeyActionStatus {

  trait Status

  case object StoppedStatus extends Status

  case object WalkingToStartStatus extends Status

  case object UpRopeStatus extends Status

  case object WalkingInRopeStatus extends Status

  case object FinalizedStatus extends Status

}