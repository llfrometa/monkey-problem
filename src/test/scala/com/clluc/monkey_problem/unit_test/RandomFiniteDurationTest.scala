package com.clluc.monkey_problem.unit_test

import com.clluc.monkey_problem.util.RandomFiniteDuration
import org.scalatest.{FunSuiteLike, Matchers}
import scala.concurrent.duration._

class RandomFiniteDurationTest extends FunSuiteLike with Matchers {

  test("generate random duration WHEN the range is 1 to 8 SHOULD return duration between 1 and 8") {
    val min = 1.second
    val max = 8.seconds
    val result = RandomFiniteDuration.next(min, max)
    result should be <= max
    result should be >= min
  }
}
