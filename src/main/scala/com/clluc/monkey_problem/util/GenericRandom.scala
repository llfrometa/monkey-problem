package com.clluc.monkey_problem.util

import scala.concurrent.duration._
import java.util.Random

/** Interface for a generator generic random values [[com.clluc.monkey_problem.util.GenericRandom]] */
trait GenericRandom[T] {
  val random = new Random

  /** Generate new random value
    *
    * @param min The minimum value of the range in which the generated value result
    * @param max The maximum value of the range in which the generated value result
    * @return The generic type T
    */
  def next(min: T, max: T): T
}

/** Generator Int random values [[com.clluc.monkey_problem.util.RandomInt]] */
object RandomInt extends GenericRandom[Int] {

  /** Generate new random value of integer
    *
    * @param min The minimum value of the range in which the generated value result
    * @param max The maximum value of the range in which the generated value result
    * @return The generated value of integer type
    *
    *  If you include [[com.clluc.monkey_problem.util.RandomInt]], you can
    *  generate integer values
    *  {{{
    *  scala> import com.clluc.monkey_problem.util.RandomInt
    *  scala> val random = RandomInt.next()
    *  random: Int = 0
    *  scala> val random = RandomInt.next()
    *  random: Int = 1
    *  }}}
    */
  override def next(min: Int = 0, max: Int = 1): Int = Math.abs(min + (random.nextInt() % (max - min + 1)))
}

/** Generator FiniteDuration random values [[com.clluc.monkey_problem.util.RandomFiniteDuration]] */
object RandomFiniteDuration extends GenericRandom[FiniteDuration] {

  /** Generate new random value of duration in seconds
    *
    * @param min The minimum value of the range in which the generated value result
    * @param max The maximum value of the range in which the generated value result
    * @return The generated value of duration type
    *
    *  If you include [[com.clluc.monkey_problem.util.RandomFiniteDuration]], you can
    *  generate integer values
    *  {{{
    *  scala> import com.clluc.monkey_problem.util.RandomFiniteDuration
    *  scala> val random = RandomFiniteDuration.next()
    *  random: Int = 2 seconds
    *  scala> val random = RandomFiniteDuration.next()
    *  random: Int = 1 second
    *  }}}
    */
  override def next(min: FiniteDuration = 1.second, max: FiniteDuration = 8.second): FiniteDuration = {
    Math.abs(min.length + (random.nextInt() % (max.length - min.length + 1))) match {
      case randomValue: Long if randomValue < min.length => min
      case randomValue: Long => randomValue.seconds
    }
  }
}