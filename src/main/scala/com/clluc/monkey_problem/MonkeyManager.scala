package com.clluc.monkey_problem

import akka.actor.SupervisorStrategy.{Escalate, Restart}
import akka.actor.{Actor, ActorLogging, ActorRef, OneForOneStrategy, Props}
import akka.pattern.ask
import com.clluc.monkey_problem.util.TimeoutParameter.Implicit._
import com.clluc.monkey_problem.util.{IdentifierGenerator, RandomFiniteDuration, RandomInt}
import scala.concurrent.Future
import scala.concurrent.duration._

/** MonkeyManager Actor [[com.clluc.monkey_problem.MonkeyManager]] */
class MonkeyManager extends Actor with ActorLogging {

  import MonkeyManagerProtocol._
  import MonkeyProtocol._

  var senderRef: ActorRef = _
  val rope: ActorRef = context.actorOf(Rope.props)

  /**
    * Supervisor of MonkeyManager Actor with maxNrOfRetries = 10 in withinTimeRange = 60 second
    */
  override val supervisorStrategy: OneForOneStrategy = OneForOneStrategy(maxNrOfRetries = 10, withinTimeRange = 60 second) {
    case e: Throwable => log.info(s"[MonkeyManager::supervisorStrategy][ERROR] = $e")
      Restart
    case _ => Escalate
  }

  override def receive: Receive = {
    case Start(monkeyTotal) =>
      senderRef = sender()
      val monkeyDataList: List[MonkeyInitialData] = getMonkeyDataSorted(monkeyTotal)
      Future.sequence(startMonkeyProcess(monkeyDataList)).map {
        case list: List[String] => list.foreach {
          id => log.info(s"[FINISH] -> Monkey(id = $id)")
        }
      }
  }

  /** Start process of distributed monkey in the rope
    *
    * @param monkeyDataList The MonkeyInitialData list generated and sorted ready for start process
    * @return Future List of Any
    */
  private def startMonkeyProcess(monkeyDataList: List[MonkeyInitialData]): List[Future[Any]] = monkeyDataList.map { md =>
    createSupervisedActor match {
      case Some(monkeyActor) => monkeyActor ? StartMonkeyProcess(md)
      case None => Future(None)
    }
  }

  /** Get a MonkeyInitialData list sorted
    *
    * @param monkeyTotal The total monkey defined in the config file
    * @return List[MonkeyInitialData]
    */
  private def getMonkeyDataSorted(monkeyTotal: Int): List[MonkeyInitialData] = {
    generateRandomDirectionAndTimer(monkeyTotal).sortWith(_.timeToStart.length < _.timeToStart.length)
  }

  /** Generator of random data, direction and time to start in the rope
    *
    * @param monkeyTotal The total monkey defined in the config file
    * @return List[MonkeyInitialData]
    */
  private def generateRandomDirectionAndTimer(monkeyTotal: Int): List[MonkeyInitialData] = {
    ((1 to monkeyTotal) map (_ => MonkeyInitialData(RandomInt.next(), RandomFiniteDuration.next()))).toList
  }

  /** Create the supervised actor child [Monkey Actor]
    *
    * @return Option[ActorRef]
    */
  private def createSupervisedActor: Option[ActorRef] = {
    val MONKEY_CHILD_PREFIX = "monkey-"
    val monkeyActorId = MONKEY_CHILD_PREFIX + IdentifierGenerator.generate
    context.child(monkeyActorId).orElse(Option(createActor(monkeyActorId)))
  }

  /** Create explicit Monkey Actor
    *
    * @param monkeyActorId The monkey actor identifier
    * @return ActorRef
    */
  private def createActor(monkeyActorId: String): ActorRef = {
    context.actorOf(Monkey.props(monkeyActorId, rope), monkeyActorId)
  }
}

/** Companion object of MonkeyManager Actor [[com.clluc.monkey_problem.MonkeyManager]] */
object MonkeyManager {

  /** Good practice defining props method of the actor
    *
    * @return Props
    */
  def props: Props = Props(classOf[MonkeyManager])
}

/** Represent the object protocol of MonkeyManager Actor [[com.clluc.monkey_problem.MonkeyManagerProtocol]] */
object MonkeyManagerProtocol {

  case class Start(monkeyTotal: Int)

}