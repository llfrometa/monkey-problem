package com.clluc.monkey_problem.unit_test

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{DefaultTimeout, ImplicitSender, TestKit, TestProbe}
import com.clluc.monkey_problem.Rope
import com.clluc.monkey_problem.RopeProtocol._
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll, FunSuiteLike, Matchers}
import scala.concurrent.duration._

class RopeTest extends TestKit(ActorSystem.create("ActorSystemMonkeyTest"))
  with FunSuiteLike with BeforeAndAfter with BeforeAndAfterAll with DefaultTimeout with ImplicitSender with Matchers {

  val monkeyId: String = "monkeyId"
  val directionGoToEast: Int = 1
  val directionGoToWest: Int = 0

  var testProbe: TestProbe = _
  var probeExpectMsg: ActorRef = _
  var probeRef: ActorRef = _
  before {
    testProbe = TestProbe()
    probeExpectMsg = testProbe.ref
    probeRef = system.actorOf(Props(classOf[Rope]))
  }

  test("send RegisterMonkey SHOULD expect RegisterMonkey message") {
    probeExpectMsg ! RegisterMonkey(monkeyId, directionGoToEast)
    testProbe.expectMsg(RegisterMonkey(monkeyId, directionGoToEast))
  }

  test("send UnRegisterMonkey SHOULD expect RegisterMonkey message") {
    probeExpectMsg ! UnRegisterMonkey(monkeyId)
    testProbe.expectMsg(UnRegisterMonkey(monkeyId))
  }

  test("send IsBusy SHOULD expect IsBusy message") {
    val direction: Int = 1
    probeExpectMsg ! IsBusy(direction)
    testProbe.expectMsg(IsBusy(direction))
  }

  test("send IsBusy SHOULD expect IsBusy message and response to sender") {
    var messages: List[Boolean] = List.empty[Boolean]
    within(500 millis) {
      probeRef ! RegisterMonkey(monkeyId, directionGoToEast)
      probeRef ! IsBusy(directionGoToWest)
      probeRef ! IsBusy(directionGoToEast)
      receiveWhile(500 millis) {
        case msg => messages = msg.asInstanceOf[Boolean] :: messages
      }
    }
    messages.length should be(2)
    messages.reverse should be(List(true, false))
  }
}
