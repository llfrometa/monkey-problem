package com.clluc.monkey_problem.unit_test

import com.clluc.monkey_problem.util.RandomInt
import org.scalatest.{FunSuiteLike, Matchers}

class RandomIntTest extends FunSuiteLike with Matchers {

  test("generate random integer WHEN the range is 0 to 1 SHOULD return number between 0 and 1") {
    val min = 0
    val max = 1
    val result = RandomInt.next(min, max)
    result should be <= max
    result should be >= min
  }
}
